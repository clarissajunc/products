#pragma once
#include "Product.h"
#include <string>

enum class NonperishableProductType
{
	Clothing,			//0
	SmallAppliances,	//1
	PersonalHygiene		//2
};

class NonperishableProduct: public Product {
private:
	NonperishableProductType m_type;

public:
	NonperishableProduct(uint32_t id, std::string name, float rawPrice, uint8_t VAT, NonperishableProductType type);
	NonperishableProductType GetType();
	uint32_t getId() override;
	std::string getName() override;
	float getRawPrice() override;
	uint32_t GetVAT();
	float GetPrice();
};
