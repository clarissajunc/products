#include <iostream>
#include <fstream>
#include "Product.h"
#include <vector>
#include <algorithm>

int main()
{
	std::ifstream productFile("Products.proddb");
	std::vector<Product> products;
	uint16_t id;
	std::string name;
	float price;
	uint16_t vat;
	std::string dateOrType;
	while (!productFile.eof())
	{
		productFile >> id >> name >> price >> vat >> dateOrType;
		products.push_back(Product(id, name, price, vat, dateOrType));

	}
	for (uint16_t i = 0; i < products.size(); i++)
	{
		if (products[i].getVat() == 19)
		{
			std::cout << products[i];
			std::cout << products[i].calculateFinalePrice();
			std::cout << "\n";
		}
	}
	int valoare;
	std::cin >> valoare;
	if (valoare)
		std::sort(products.begin(), products.end(), Product::compareName);
	else
		std::sort(products.begin(), products.end(), Product::comparePrice);

	for (uint16_t i = 0; i < products.size(); i++)
	{
		std::cout << products[i];
		std::cout << "\n";
	}
	return 0;
}





