#include "Product.h"

bool Product::compareName(Product* lhs, Product* rhs)
{
	return lhs->getName() < rhs->getName();
}

bool Product::compareRawPrice(Product* lhs, Product* rhs)
{
	return lhs->getRawPrice() < rhs->getRawPrice();
}

