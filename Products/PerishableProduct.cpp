#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(uint32_t id, std::string name, float rawPrice, uint8_t VAT, std::string expirationDate)
	: Product(id, name, rawPrice, VAT), m_expirationDate(expirationDate)
{
	//empty
}

Product::Product(uint32_t id, std::string name, float rawPrice, const uint32_t VAT):
	m_id(id), m_name(name), m_rawPrice(rawPrice), m_VAT(VAT)
{
	//EMPTY
}

uint32_t PerishableProduct::getId()
{
	return this->m_id;
}

std::string PerishableProduct::getName()
{
	return this->m_name;
}

float PerishableProduct::getRawPrice()
{
	return this->m_rawPrice;
}

std::string PerishableProduct::getExpirationDate()
{
	return this->m_expirationDate;
}

uint32_t PerishableProduct::GetVAT()
{
	return this->m_VAT;
}

float PerishableProduct::GetPrice()
{
	return this->m_rawPrice+this->m_VAT*this->m_rawPrice;
}
