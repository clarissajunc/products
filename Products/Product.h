#pragma once
#include <cstdint>
#include <string>
#include <iostream>
#include <algorithm>
#include "IPriceable.h"

class Product: public IPriceable
{
public:
	Product(uint32_t id, std::string m_name, float m_rawPrice, const uint32_t VAT);
	virtual uint32_t getId()=0;
	virtual std::string getName()=0;
	virtual float getRawPrice()=0;
	static bool compareName(Product* lhs, Product* rhs);
	static bool compareRawPrice(Product* lhs, Product* rhs);

protected:
	uint32_t m_id; 
	std::string m_name;
	float m_rawPrice;
	const uint32_t m_VAT;
};