#include <iostream>
#include <fstream>
#include "Product.h"
#include "PerishableProduct.h"
#include "NonPerishableProduct.h"
#include <vector>
#include <algorithm>

void printProducts(std::vector<Product*> products)
{
	for (uint8_t i = 0; i < products.size(); i++)
	{
		std::cout << products[i]->getId() << " ";
		std::cout << products[i]->getName() << " ";
		std::cout << products[i]->getRawPrice() << " ";
		std::cout << products[i]->GetVAT() << " ";
		if (dynamic_cast<NonperishableProduct*>(products[i]) == nullptr)
			std::cout << (static_cast<PerishableProduct*>(products[i]))->getExpirationDate();
		else
			if ((static_cast<NonperishableProduct*>(products[i]))->GetType() == NonperishableProductType::Clothing)
				std::cout << "Clothing";
			else if ((static_cast<NonperishableProduct*>(products[i]))->GetType() == NonperishableProductType::SmallAppliances)
				std::cout << "SmallAppliances";
			else
				std::cout << "PersonalHygiene";
		std::cout << std::endl;
	}
}
void sortByChoice(int valueOfChoice, std::vector<Product*>& products)
{
	std::cin >> valueOfChoice;
	if (valueOfChoice)
		std::sort(products.begin(), products.end(), Product::compareName);
	else
		std::sort(products.begin(), products.end(), Product::compareRawPrice);
}
int main()
{
	std::vector<Product*> products;
	std::ifstream readFile("Products.proddb");
	while (!readFile.eof()) {
		uint32_t id;
		std::string name;
		float rawPrice;
		uint32_t VAT;
		readFile >> id >> name >> rawPrice >> VAT;
		if (VAT == 9)
		{
			std::string expirationDate;
			readFile >> expirationDate;
			products.push_back(new PerishableProduct(id, name, rawPrice, VAT, expirationDate));
		}
		else
		{
			std::string stringType;
			NonperishableProductType type;
			readFile >> stringType;
			if (stringType == "Clothing")
				products.push_back(new NonperishableProduct(id, name, rawPrice, VAT, NonperishableProductType::Clothing));
			else if (stringType == "SmallAppliances")
				products.push_back(new NonperishableProduct(id, name, rawPrice, VAT, NonperishableProductType::SmallAppliances));
			else
				products.push_back(new NonperishableProduct(id, name, rawPrice, VAT, NonperishableProductType::PersonalHygiene));
		}

	}
	printProducts(products);
	int valueOfChoice = 0;
	sortByChoice(valueOfChoice, products);
	printProducts(products);
	return 0;
}





