#pragma once
#include "Product.h"
#include <string>

class PerishableProduct : public Product {
private:
	std::string m_expirationDate;

public:
	PerishableProduct(uint32_t id, std::string name, float rawPrice, const uint8_t VAT, std::string expirationDate);
	std::string getExpirationDate();
	uint32_t getId() override;
	std::string getName() override;
	float getRawPrice() override;
	uint32_t GetVAT();
	float GetPrice();
};