#pragma once
#include <cstdint>
class IPriceable {
public:
	virtual uint32_t GetVAT() = 0;
	virtual float GetPrice() = 0;
};