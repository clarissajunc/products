#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(uint32_t id, std::string name, float rawPrice, uint8_t VAT, NonperishableProductType  type):
	Product(id, name, rawPrice, VAT), m_type(type)
{
	//empty
}

NonperishableProductType NonperishableProduct::GetType()
{
	return m_type;
}


uint32_t NonperishableProduct::GetVAT()
{
	return this->m_VAT;
}

uint32_t NonperishableProduct::getId()
{
	return this->m_id;
}

std::string NonperishableProduct::getName()
{
	return this->m_name;
}

float NonperishableProduct::getRawPrice()
{
	return this->m_rawPrice;
}

float NonperishableProduct::GetPrice()
{
	return this->m_rawPrice+this->m_VAT/100*this->m_rawPrice;
}
